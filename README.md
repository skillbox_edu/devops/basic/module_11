# Практическая работа по модулю "Мониторинг" курса "DevOps-инженер. Основы"

## Цели задания
* Закрепить на практике знания, которые вы получили в данном модуле.
* Научиться разрабатывать систему мониторинга, релевантную для вашей инфраструктуры.
* Научиться предугадывать потенциальные ботлнеки для вашей системы.

Для выполнения данной задачи мы будем использовать Grafana, Prometheus, Loki, Prometheus Alertmanager.

## Что входит в задание
1. Собрать необходимые метрики.
1. Реализовать систему мониторинга.
1. Сделать алерты.
1. Создать полноценную систему мониторинга (по желанию).

## Установка и настройка

1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/basic/module_11.git
   cd module_11
   ```
   
1. Запускаем контейнеры
   ```shell
   docker-compose up -d
   docker-compose ps
   ```
   
   * [Prometheus](http://127.0.0.1:9090)
   * [Node Exporter](http://127.0.0.1:9100)
   * [Alertmanager](http://127.0.0.1:9093)
   * [Grafana](http://127.0.0.1:3000)
   
1. Задание 1. Сбор необходимых метрик
    
   * `node_cpu_seconds_total` - режим `iowait`.   
   
     ```
     node_cpu_seconds_total{mode="iowait"}
     ```
     
     ![node_cpu_seconds_total - режим iowait](docs/imgs/task-1-node-cpu-seconds-total-iowait.png)
   
   * `node_cpu_seconds_total` - загрузка процессора в процентах (без CPU).
   
     ```
     100 - avg(irate(node_cpu_seconds_total{mode="idle"}[5m])) without (cpu) * 100
     ```
     
     ![node_cpu_seconds_total - загрузка процессора в процентах](docs/imgs/task-1-node-cpu-load.png)
   
   * `node_filesystem_avail_bytes` - занятое место в процентах (`mountpoin="/"`, исключить `device tmpfs`).
   
     ```
     100 - node_filesystem_avail_bytes{mountpoint="/", device!="tmpfs"} / node_filesystem_size_bytes * 100
     ```
     
     ![node_filesystem_avail_bytes - занятое место в процентах](docs/imgs/task-1-disk-usage.png)
    
   * `node_load` за 15 минут.
   
     ```
     node_load15
     ```
     
     ![node_load за 15 минут](docs/imgs/task-1-node-load-15m.png)
    
1. Задание 2. Реализация системы мониторинга

   Дашборды можно импортировать из JSON-файлов в папке [dashboards](grafana/dashboards).

   * Источник данных - Prometheus:
   
     ![Prometheus Data Source](docs/imgs/task-2-prometheus-data-source.png)

   * Дашборд с метриками из задания 1:

     ![Дашборд с метриками из задания 1](docs/imgs/task-2-task-1-metrics.png)

   * Дашборд с собственными метриками:

     ![Дашборд с собственными метриками](docs/imgs/task-2-own-metrics.png)
    
1. Задание 3. Алертинг

   Предварительно нужно создать [чат-бота и группу в Telegram](#как-получать-оповещения-от-alertmanager-в-telegram). Затем прописать настройки в секции `receivers` файла [alertmanager.yml](alertmanager/alertmanager.yml).

   ```yaml
   route:
     # ...
     receiver: '<you receiver name>'
   receivers:
     - name: '<you receiver name>'
       telegram_configs:
         - bot_token: '<you telegramt-bot token>'
           api_url: 'https://api.telegram.org'
           chat_id: <you telegram chat id>
   ```

   Пример срабатывания алерта при падении таргета:

   * Prometheus
    
     ![Prometheus Target Down Alert](docs/imgs/task-3-prometheus-target-alerting.png)
    
   * Alertmanager
    
     ![Prometheus Target Down Alert](docs/imgs/task-3-alertmanager-target-alerting.png)
    
   * Telegram
    
     ![Telegram Target Down Alert](docs/imgs/task-3-telegram-target-alerting.png)
    
   Пример срабатывания алерта при загрузке ЦП:

   * Prometheus
    
     ![Prometheus CPU Load Alert](docs/imgs/task-3-prometheus-cpu-load-alerting.png)
    
   * Alertmanager
    
     ![Prometheus CPU Load Alert](docs/imgs/task-3-alertmanager-cpu-load-alerting.png)
    
   * Telegram
    
     ![Telegram CPU Load Alert](docs/imgs/task-3-telegram-cpu-load-alerting.png)

## Полезные материалы

* [Документация Prometheus](https://prometheus.io/docs/introduction/overview/)
* [Коллекция готовых алертов для Alertmanager](https://awesome-prometheus-alerts.grep.to/rules.html)
* [Prometheus + Grafana + Alertmanager в Docker](https://www.dmosk.ru/miniinstruktions.php?mini=prometheus-stack-docker). На русском.
* [Monitoring a Linux host with Prometheus, Node Exporter, and Docker Compose](https://grafana.com/docs/grafana-cloud/quickstart/docker-compose-linux/). Официальная документация Grafana.
* Пример [docker-compose.yml](https://github.com/PagerTree/prometheus-grafana-alertmanager-example/blob/master/docker-compose.yml) для Prometheus + Node Exporter + Grafana + Alertmanager.
* [Официальный репозиторий Prometheus](https://github.com/prometheus). Примеры запуска контейнеров лучше смотреть здесь, чем на Docker Hub.
* [Образ Prometheus](https://hub.docker.com/r/prom/prometheus)
* [Образ Node Exporter](https://hub.docker.com/r/prom/node-exporter)
* [Образ Alertmanager](https://hub.docker.com/r/prom/alertmanager)
* [Образ Grafana](https://hub.docker.com/r/grafana/grafana)
* [Осваиваем мониторинг с Prometheus. Часть 2. PromQL и метки](https://laurvas.ru/prometheus-p2/). Есть раздел, как считать загрузку процессора (внизу).
* [Визуализация данных в Grafana](https://grafana.com/docs/grafana/latest/panels-visualizations/visualizations/)
* [Export дашбордов в Grafana](https://grafana.com/docs/grafana/v9.0/dashboards/export-import/)
* [Network interface metrics from the node exporter](https://www.robustperception.io/network-interface-metrics-from-the-node-exporter/). Метрики трафика.
* [Стресс-тестирование систем в Linux – утилита stress-ng](https://itproffi.ru/stress-testirovanie-sistem-v-linux-utilita-stress-ng/). Пример загрузки ЦП на 100% в течение 2-х минут `stress-ng --cpu 2 --timeout 2m`.

## <a id="как-получать-оповещения-от-alertmanager-в-telegram" />Как получать оповещения от Alertmanager в Telegram

Информация взята из открытых источников:
* [How to configure Prometheus Alertmanager to send alerts to Telegram](https://velenux.wordpress.com/2022/09/12/how-to-configure-prometheus-alertmanager-to-send-alerts-to-telegram/)
* [Prometheus Alertmanager send alerts via Telegram](https://tienbm90.medium.com/prometheus-alertmanager-send-alerts-to-admin-via-telegram-81b4f547089f)
* [Ответ на StackOverflow](https://stackoverflow.com/a/32572159)
* [Конфиг Alertmanager для Telegram](https://prometheus.io/docs/alerting/latest/configuration/#telegram_config)

Настройка телеграм-бота:
1. Найти бота `BotFather`.
1. Создать нового бота, запомнить токен.
1. Создать группу и добавить туда созданного бота. Сделать бота админом группы.
1. Перейти по ссылке `https://api.telegram.org/bot<token>/getUpdates`.
1. Если в ответ получено `{"ok":true,"result":[]}`, то нужно удалить бота из группы и добавить туда заново. Если же список `result` не пуст, то скопировать оттуда `chat_id` (обязательно со знаком минус (`-`), это часть id).
